import os
from pathlib import Path
from typing import Final

from dotenv import load_dotenv

from pynsee.utils import clear_all_cache
from pynsee.utils.init_conn import init_conn

UTF_8: Final[str] = "utf-8"

# Env
load_dotenv()
INSEE_API_KEY = os.getenv("INSEE_API_KEY")
INSEE_API_SECRET = os.getenv("INSEE_API_SECRET")
ANS_PWD = os.getenv("ANS_PWD")
ANS_MAIL = os.getenv("ANS_MAIL")

# Paths
ROOT_PATH: Final[Path] = Path(__file__).parent.parent.absolute()
DIR2FIGURES: Final[Path] = ROOT_PATH / "images"
DIR2FIGURES.mkdir(parents=True, exist_ok=True)
DIR2DATA: Final[Path] = ROOT_PATH / "data"
DIR2EXPORTS: Final[Path] = DIR2DATA / "exports"
DIR2EXPORTS.mkdir(parents=True, exist_ok=True)

# clean cache if keys have changed as recommended by pynsee

# init connection

clear_all_cache()
con = init_conn(insee_key=INSEE_API_KEY, insee_secret=INSEE_API_SECRET)

# API constants
## [SMT](https://smt.esante.gouv.fr/api-docs/#/API)

BASE_URL_SMT = "https://smt.esante.gouv.fr"
