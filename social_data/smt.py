import requests
from social_data.constants import BASE_URL_SMT


def list_terminologies(access_token: str = ""):

    r = requests.get(
        url=BASE_URL_SMT + f"/api/terminologies/list?accessToken={access_token}",
        headers={"accept": "*/*"},
        verify=False,
    )

    return r.json()
