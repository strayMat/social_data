from social_data.constants import ANS_MAIL, ANS_PWD, BASE_URL_SMT
import requests

BASE_URL_ANS = "https://smt.esante.gouv.fr/"


def connect_ans(username: str = ANS_MAIL, pwd: str = ANS_PWD):
    payload = {
        "password": pwd,
        "username": username,
        "client_id": "user-api",
        "grant_type": "password",
    }
    # "refresh_token": "ey...",
    headers = {
        "accept": "*/*",
        "Content-Type": "application/x-www-form-urlencoded",
    }
    logging_url = (
        BASE_URL_SMT + "/ans/sso/auth/realms/ANS/protocol/openid-connect/token"
    )
    print(logging_url)
    r = requests.post(
        url=logging_url,
        headers=headers,
        data=payload,
        verify=False,
    )

    if r.status_code == 200:
        return r.json()
    else:
        r
