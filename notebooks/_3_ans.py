# %%
from social_data.logging import connect_ans
from social_data.smt import list_terminologies

# %%
con = connect_ans()
print(con)

# %%
available_terminologies = list_terminologies(access_token=con["access_token"])
available_terminologies

# %%
print(len(available_terminologies))
for t in available_terminologies:
    print(t["title"])
    print(t["terminologyId"])

# %%
con["access_token"]
# %%
