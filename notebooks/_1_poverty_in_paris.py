# ### Example at: https://pynsee.readthedocs.io/en/latest/examples/example_poverty_paris_urban_area.html ### #

from social_data.constants import con
from pynsee.localdata import *

import pandas as pd
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import descartes
import geopandas as gpd

# get a list all data available : datasets and variables
metadata = get_local_metadata()

# geographic metadata
nivgeo = get_nivgeo_list()

# get geographic area list
area = get_area_list()

# get all communes in Paris urban area
areaParis = get_included_area("unitesUrbaines2020", ["00851"])

# get selected communes identifiers
code_com_paris = areaParis.code.to_list()

# get numeric values from INSEE database
dataParis = get_local_data(
    dataset_version="GEO2020FILO2017",
    variables="INDICS_FILO_DISP_DET",
    nivgeo="COM",
    geocodes=code_com_paris,
)

# select poverty rate data, exclude paris commune
data_plot = dataParis.loc[dataParis.UNIT == "TP60"]
data_plot = data_plot.loc[data_plot.CODEGEO != "75056"]
# %%*

# get communes limits
map_com = gpd.read_file(get_map_link("communes"))
map_arr_mun = gpd.read_file(get_map_link("arrondissements-municipaux"))
map_idf = pd.concat([map_com, map_arr_mun])

# %%
mapparis = map_idf.merge(data_plot, how="right", left_on="code", right_on="CODEGEO")

# plot
fig, ax = plt.subplots(1, 1, figsize=[15, 15])
mapparis.plot(
    column="OBS_VALUE", cmap=cm.viridis, legend=True, ax=ax, legend_kwds={"shrink": 0.3}
)
ax.set_axis_off()
ax.set(title="Poverty rate in Paris urban area in 2017")
plt.show()
fig.savefig(
    DIR2FIGURES / "poverty_paris_urban_area.svg",
    format="svg",
    dpi=1200,
    bbox_inches="tight",
    pad_inches=0,
)

# %%
