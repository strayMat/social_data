# %%
from social_data.constants import con

# %% [markdown]
# # Get macroeconomuc data with approriate api key
# %%
from pynsee.macrodata import *
from pynsee.localdata import *
import pandas as pd
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
import geopandas as gpd

# get macroeconomic datasets list
# %%
# all_series = get_series_list()
all_ds = get_dataset_list()

# %%
# idbank_ipc = get_series_list("IPC-2015", "CLIMAT-AFFAIRES")

# %%
map = gpd.read_file(get_map_link("departements"))
areas = get_area_list()
# %%
# Explore some interesting series

# TCRED-SANTE-EQUIP-HOP, equipement de santé
equip_sante_df = get_series(["TCRED-SANTE-EQUIP-HOP"])
